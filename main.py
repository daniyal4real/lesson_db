# import psycopg2
# from config import *
#
# try:
#     #connect to the existing database
#     connection = psycopg2.connect(
#         host=host,
#         user=user,
#         password=password,
#         database=db_name
#     )
#     connection.autocommit = True
#     #the cursor for performing database operations
#     with connection.cursor() as cursor:
#         cursor.execute(
#             "SELECT version();"
#         )
#         print(f"Server version: {cursor.fetchone()}")
#
#     #creating table
#     # with connection.cursor() as cursor:
#     #     cursor.execute(
#     #         "CREATE TABLE users("
#     #         "id SERIAL PRIMARY KEY,"
#     #         "first_name VARCHAR(50) NOT NULL,"
#     #         "nick_name VARCHAR(50) NOT NULL);"
#     #     )
#     #     #connection commit
#     #     print("[INFO] Table was created successfully")
#
#     #insert data
#     # with connection.cursor() as cursor:
#     #     cursor.execute(
#     #         "INSERT INTO users(first_name, nick_name) VALUES"
#     #         "('Alex', 'player1');"
#     #
#     #     )
#     #     #commit
#     #     print("[INFO] data was inserted succesfully")
#
#     # with connection.cursor() as cursor:
#     #     cursor.execute(
#     #         "SELECT nick_name FROM users WHERE first_name = 'Alex';"
#     #     )
#     #     print(cursor.fetchone())
#     #     #commit
#     #     print("[INFO] data was selected successfully")
#
#     with connection.cursor() as cursor:
#         cursor.execute(
#             "DROP TABLE users"
#         )
#         #commit
#         print("[INFO] Table was dropped")
#
#
# except Exception as _ex:
#     print("[INFO] Error while working with Postgres", _ex)
#
# finally:
#     if connection:
#         connection.close()
#         print("[INFO] PostgreSQL connection closed")
#
#
